from sqlalchemy.orm import Session

from database.models.organization import Organization
from schemas import organization as schemas

def get_organization(db: Session, id: int):
    return db.query(Organization).filter(Organization.id == id).first()

def store_organization(db: Session, organization: schemas.OrganizationCreate):
    db_org = Organization(
        name = organization.name,
        contact_email = organization.contact_email,
        address = organization.address
    )

    db.add(db_org)
    db.commit()
    db.refresh(db_org)
    return db_org
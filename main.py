from fastapi import FastAPI
from mangum import Mangum

from routers import organization
from database.database import engine, Base

Base.metadata.create_all(bind=engine)

app = FastAPI()

app.include_router(organization.router)

# Setup Lambda handler
handler = Mangum(app)
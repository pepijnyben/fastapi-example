## Prerequisites
- Docker
- Docker Compose

## Installation

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install dependencies

```bash
pip install -r requirements.txt     # Optional
docker-compose up -d                # Start local environment, including MySQL database
docker-compose exec web pytest .    # Run unit tests
```

from pydantic import BaseModel, EmailStr

class OrganizationBase(BaseModel):
    name: str
    contact_email: EmailStr
    address: str

class OrganizationCreate(OrganizationBase):
    pass

class Organization(OrganizationBase):
    id: int
    class Config:
        orm_mode = True
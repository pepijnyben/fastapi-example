FROM python:3.9

WORKDIR /app

# Copy all working files to app folder
COPY ./ /app

# Install all requirements
RUN pip install --no-cache-dir --upgrade -r /app/requirements.txt

# Run uvicorn server on port 80
CMD ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "80"]
from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session

from schemas import organization as schemas
from database.database import SessionLocal
from services import organization as service

def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()

router = APIRouter(
    prefix="/organization",
    tags=["organization"]
)

@router.get("/{id}", response_model = schemas.Organization)
async def show_organization(id: int, db: Session = Depends(get_db)):
    db_org = service.get_organization(db, id)
    if db_org is None:
        raise HTTPException(status_code = 404, detail="Organization not found")
    return db_org

@router.post("/", response_model = schemas.Organization)
async def store_organization(organization: schemas.OrganizationCreate, db: Session = Depends(get_db)):
    return service.store_organization(db, organization)
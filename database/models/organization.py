from sqlalchemy import Boolean, Column, Integer, String

from ..database import Base

class Organization(Base):
    __tablename__ = "organizations"

    id = Column(Integer, primary_key=True, index=True, autoincrement=True)
    name = Column(String(255), unique=True, index=True)
    contact_email = Column(String(255))
    address = Column(String(255))

import pytest
from fastapi.testclient import TestClient
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from database.database import Base
from main import app
from routers.organization import get_db

SQLALCHEMY_DATABASE_URL = "sqlite:///./test.db"

engine = create_engine(
    SQLALCHEMY_DATABASE_URL, connect_args={"check_same_thread": False}
)
TestingSessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

@pytest.fixture()
def test_db():
    Base.metadata.create_all(bind=engine)
    yield
    Base.metadata.drop_all(bind=engine)

def override_get_db():
    try:
        db = TestingSessionLocal()
        yield db
    finally:
        db.close()

app.dependency_overrides[get_db] = override_get_db

client = TestClient(app)

def test_create_organization(test_db):
    test_request_payload = {"name": "Test Org", "contact_email": "test@org.com", "address": "Bovensingel 1"}
    test_response_payload = {"id": 1, "name": "Test Org", "contact_email": "test@org.com", "address": "Bovensingel 1"}

    response = client.post(
        "/organization/",
        json=test_request_payload
    )

    assert response.status_code == 200
    assert response.json() == test_response_payload